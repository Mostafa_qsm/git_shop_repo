-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2019 at 07:38 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snaland_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `access_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `ac_admin` tinyint(1) NOT NULL,
  `ac_accoountant` tinyint(1) NOT NULL,
  `ac_orders` tinyint(1) NOT NULL,
  `ac_customers` tinyint(1) NOT NULL,
  `ac_reports` tinyint(1) NOT NULL,
  `ac_settings` tinyint(1) NOT NULL,
  `ac_frontend` tinyint(1) NOT NULL,
  `ac_account` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `email` longtext NOT NULL,
  `password` longtext NOT NULL,
  `sex` longtext NOT NULL,
  `phone` longtext NOT NULL,
  `address` longtext NOT NULL,
  `pic` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank_portal`
--

CREATE TABLE IF NOT EXISTS `bank_portal` (
  `bank_potal_id` bigint(20) NOT NULL,
  `name` longtext NOT NULL,
  `pic` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL,
  `pic` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comments_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `commodity` longtext NOT NULL,
  `designing` longtext NOT NULL,
  `quality` longtext NOT NULL,
  `performance` longtext NOT NULL,
  `cost` longtext NOT NULL,
  `subject` longtext NOT NULL,
  `strengths` longtext NOT NULL,
  `weak_points` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `email` longtext NOT NULL,
  `password` longtext NOT NULL,
  `sex` longtext NOT NULL,
  `phone` longtext NOT NULL,
  `province` longtext NOT NULL,
  `eparchy` longtext NOT NULL,
  `postal_code` longtext NOT NULL,
  `pic` longtext NOT NULL,
  `newsletters` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `discount_coupon`
--

CREATE TABLE IF NOT EXISTS `discount_coupon` (
  `discount_coupon_id` bigint(20) NOT NULL,
  `type_of_discount_id` bigint(20) NOT NULL,
  `value` bigint(20) NOT NULL,
  `code` longtext NOT NULL,
  `description` longtext NOT NULL,
  `expiration_date` date NOT NULL,
  `minimum_cost` bigint(20) NOT NULL,
  `maximum_cost` bigint(20) NOT NULL,
  `individual_use` tinyint(1) NOT NULL,
  `except_special_products` tinyint(1) NOT NULL,
  `count_usage_number` int(11) NOT NULL,
  `number_of_items_in_the_basket` int(11) NOT NULL,
  `for_each_user` int(11) NOT NULL,
  `authorized_emails` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `email_setting_id` bigint(20) NOT NULL,
  `subject` longtext NOT NULL,
  `from_email` longtext NOT NULL,
  `to email` longtext NOT NULL,
  `email_header_text` longtext NOT NULL,
  `email_footer_text` longtext NOT NULL,
  `colore_text` longtext NOT NULL,
  `colore_footer` longtext NOT NULL,
  `colore_hader` longtext NOT NULL,
  `date_of_registration` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `emp_id` int(11) NOT NULL,
  `emp_code` longtext NOT NULL,
  `role_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `email` longtext NOT NULL,
  `password` longtext NOT NULL,
  `sex` longtext NOT NULL,
  `phone` longtext NOT NULL,
  `address` longtext NOT NULL,
  `note` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `front_general_settings`
--

CREATE TABLE IF NOT EXISTS `front_general_settings` (
  `front_general_setting_id` bigint(20) NOT NULL,
  `name` longtext NOT NULL,
  `email` longtext NOT NULL,
  `phone1` int(20) NOT NULL,
  `phone2` int(20) NOT NULL,
  `address` longtext NOT NULL,
  `fax` longtext NOT NULL,
  `facebook` longtext NOT NULL,
  `youtoub` longtext NOT NULL,
  `instagram` longtext NOT NULL,
  `appstor` longtext NOT NULL,
  `googleplay` longtext NOT NULL,
  `header_text` longtext NOT NULL,
  `description` longtext NOT NULL,
  `logo` longtext NOT NULL,
  `about` longtext NOT NULL,
  `terms_and_conditions` longtext NOT NULL,
  `privacy` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `front_slider_settings`
--

CREATE TABLE IF NOT EXISTS `front_slider_settings` (
  `front_slider_setting_id` bigint(20) NOT NULL,
  `no_slider` int(20) NOT NULL,
  `pic` longtext NOT NULL,
  `subject` longtext NOT NULL,
  `description` longtext NOT NULL,
  `product_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `gallery_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `pic` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grouping_products`
--

CREATE TABLE IF NOT EXISTS `grouping_products` (
  `group_product_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grouping_products`
--

INSERT INTO `grouping_products` (`group_product_id`, `name`, `description`) VALUES
(1, 'محصول ساده', ''),
(2, 'محصول متغییر', ''),
(3, 'محصول مکمل', ''),
(4, 'محصول گروه بندی شده', '');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

CREATE TABLE IF NOT EXISTS `invoice_product` (
  `invoice_product_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `title` longtext NOT NULL,
  `description` longtext NOT NULL,
  `amount` int(11) NOT NULL,
  `amunt_paid` bigint(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE IF NOT EXISTS `labels` (
  `lable_id` int(11) NOT NULL,
  `subject` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lables_products`
--

CREATE TABLE IF NOT EXISTS `lables_products` (
  `lable_product_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `name` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_settings`
--

CREATE TABLE IF NOT EXISTS `payment_settings` (
  `peyment_setting` bigint(20) NOT NULL,
  `bank_portal_id` bigint(20) NOT NULL,
  `link` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL,
  `group_product_id` bigint(20) NOT NULL,
  `name_product` longtext NOT NULL,
  `description` longtext NOT NULL,
  `note` longtext NOT NULL,
  `lable_id` longtext NOT NULL,
  `brand_id` bigint(20) NOT NULL,
  `pic` longtext NOT NULL,
  `study` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products_specifications_taxonomy`
--

CREATE TABLE IF NOT EXISTS `products_specifications_taxonomy` (
  `specifications_taxonomy_id` bigint(20) NOT NULL,
  `specification_id` bigint(20) NOT NULL,
  `description` varchar(200) NOT NULL,
  `parent` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_specifications_taxonomy`
--

INSERT INTO `products_specifications_taxonomy` (`specifications_taxonomy_id`, `specification_id`, `description`, `parent`) VALUES
(1, 1, '', 0),
(2, 2, '', 1),
(3, 3, '', 1),
(4, 4, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products_specification_key`
--

CREATE TABLE IF NOT EXISTS `products_specification_key` (
  `specification_key_id` bigint(20) NOT NULL,
  `naem` varchar(200) NOT NULL,
  `term_id` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_specification_key`
--

INSERT INTO `products_specification_key` (`specification_key_id`, `naem`, `term_id`) VALUES
(1, 'مشخصات فیزیکی', 3),
(2, 'ابعاد', 3),
(3, 'وزن', 3),
(4, 'لوازم جانبی', 3);

-- --------------------------------------------------------

--
-- Table structure for table `products_specification_value`
--

CREATE TABLE IF NOT EXISTS `products_specification_value` (
  `product_value_id` int(11) NOT NULL,
  `product_name` longtext NOT NULL,
  `term_id` bigint(20) NOT NULL,
  `specification_key_id` bigint(20) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `property_key`
--

CREATE TABLE IF NOT EXISTS `property_key` (
  `property_key_id` bigint(20) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `property_key_products`
--

CREATE TABLE IF NOT EXISTS `property_key_products` (
  `property_key_product_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `property_value`
--

CREATE TABLE IF NOT EXISTS `property_value` (
  `property_value_id` bigint(20) NOT NULL,
  `property_key_id` bigint(20) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `property_value_products`
--

CREATE TABLE IF NOT EXISTS `property_value_products` (
  `property_value_product_id` bigint(20) DEFAULT NULL,
  `property_key_product_id` bigint(20) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `related_products`
--

CREATE TABLE IF NOT EXISTS `related_products` (
  `related_product_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `related_product` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `rol_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`rol_id`, `name`, `description`) VALUES
(1, 'workaday', '...'),
(2, 'accountant', '...'),
(3, 'stack_clerk', '...'),
(4, 'customer_order', '...'),
(5, 'admin', '...');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL,
  `type` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `type`, `description`) VALUES
(1, 'system_name', 'Sanaland Management System'),
(2, 'system_title', 'Sanaland Management System'),
(3, 'address', 'Address'),
(4, 'phone', '+7878'),
(5, 'currency', 'usd'),
(6, 'system_email', 'sanaland@sanaland.com'),
(7, 'language', 'english'),
(8, 'text-align', 'left-to right'),
(9, 'skin_color', 'default'),
(10, 'running_year', '2018-2019');

-- --------------------------------------------------------

--
-- Table structure for table `simple_products`
--

CREATE TABLE IF NOT EXISTS `simple_products` (
  `simple_product_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `price` int(20) NOT NULL,
  `special_price` int(20) NOT NULL,
  `From_date_of_sale` date NOT NULL,
  `Until_date_of_sale` date NOT NULL,
  `slake` bigint(20) NOT NULL,
  `stock_inventory` int(20) NOT NULL,
  `transportation_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `status_orders`
--

CREATE TABLE IF NOT EXISTS `status_orders` (
  `st_or_id` int(11) NOT NULL,
  `subject` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_orders`
--

INSERT INTO `status_orders` (`st_or_id`, `subject`, `description`) VALUES
(1, 'درصف بررسی', ''),
(2, 'تایید و آماره سازی سفارش ', ''),
(3, 'تحویل به مامور پست', ''),
(4, 'تحویل به مشتری', ''),
(5, 'مسترد شده', ''),
(6, 'لغو شده', '');

-- --------------------------------------------------------

--
-- Table structure for table `status_publication`
--

CREATE TABLE IF NOT EXISTS `status_publication` (
  `st_pu_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_publication`
--

INSERT INTO `status_publication` (`st_pu_id`, `name`, `description`) VALUES
(1, 'منتشر شده', ''),
(2, 'منتشر نشده', '');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE IF NOT EXISTS `terms` (
  `term_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`term_id`, `name`, `description`) VALUES
(1, 'ارایشی', ''),
(2, 'اقایان', ''),
(3, 'الکترونیک', '');

-- --------------------------------------------------------

--
-- Table structure for table `terms_products`
--

CREATE TABLE IF NOT EXISTS `terms_products` (
  `term_prdouct_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `term_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `term_taxonomy` (
  `term_taxonomy_id` bigint(20) NOT NULL,
  `term_id` bigint(20) NOT NULL,
  `taxonomy` varchar(32) NOT NULL,
  `description` longtext NOT NULL,
  `parent` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `term_taxonomy`
--

INSERT INTO `term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`) VALUES
(1, 1, '', '', 0),
(2, 2, '', '', 1),
(3, 3, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transportation_settings`
--

CREATE TABLE IF NOT EXISTS `transportation_settings` (
  `transportation_id` bigint(20) NOT NULL,
  `name` longtext NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `type_of_discount`
--

CREATE TABLE IF NOT EXISTS `type_of_discount` (
  `type_of_discount_id` bigint(20) NOT NULL,
  `name` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `variable_products`
--

CREATE TABLE IF NOT EXISTS `variable_products` (
  `variable_product_id` bigint(20) NOT NULL,
  `property_value_prduct_id` bigint(20) NOT NULL,
  `price` int(11) NOT NULL,
  `special_price` int(11) NOT NULL,
  `From_date_of_sale` date NOT NULL,
  `Until_date_of_sale` date NOT NULL,
  `slake` bigint(20) NOT NULL,
  `stock_inventory` int(11) NOT NULL,
  `transportation_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`access_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bank_portal`
--
ALTER TABLE `bank_portal`
  ADD PRIMARY KEY (`bank_potal_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
  ADD PRIMARY KEY (`discount_coupon_id`);

--
-- Indexes for table `email_settings`
--
ALTER TABLE `email_settings`
  ADD PRIMARY KEY (`email_setting_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `front_general_settings`
--
ALTER TABLE `front_general_settings`
  ADD PRIMARY KEY (`front_general_setting_id`);

--
-- Indexes for table `front_slider_settings`
--
ALTER TABLE `front_slider_settings`
  ADD PRIMARY KEY (`front_slider_setting_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `grouping_products`
--
ALTER TABLE `grouping_products`
  ADD PRIMARY KEY (`group_product_id`);

--
-- Indexes for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD PRIMARY KEY (`invoice_product_id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`lable_id`);

--
-- Indexes for table `lables_products`
--
ALTER TABLE `lables_products`
  ADD PRIMARY KEY (`lable_product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `products_specifications_taxonomy`
--
ALTER TABLE `products_specifications_taxonomy`
  ADD PRIMARY KEY (`specifications_taxonomy_id`);

--
-- Indexes for table `products_specification_key`
--
ALTER TABLE `products_specification_key`
  ADD PRIMARY KEY (`specification_key_id`);

--
-- Indexes for table `products_specification_value`
--
ALTER TABLE `products_specification_value`
  ADD PRIMARY KEY (`product_value_id`);

--
-- Indexes for table `property_key`
--
ALTER TABLE `property_key`
  ADD PRIMARY KEY (`property_key_id`);

--
-- Indexes for table `property_key_products`
--
ALTER TABLE `property_key_products`
  ADD PRIMARY KEY (`property_key_product_id`);

--
-- Indexes for table `property_value`
--
ALTER TABLE `property_value`
  ADD PRIMARY KEY (`property_value_id`);

--
-- Indexes for table `related_products`
--
ALTER TABLE `related_products`
  ADD PRIMARY KEY (`related_product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `simple_products`
--
ALTER TABLE `simple_products`
  ADD PRIMARY KEY (`simple_product_id`);

--
-- Indexes for table `status_orders`
--
ALTER TABLE `status_orders`
  ADD PRIMARY KEY (`st_or_id`);

--
-- Indexes for table `status_publication`
--
ALTER TABLE `status_publication`
  ADD PRIMARY KEY (`st_pu_id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`term_id`);

--
-- Indexes for table `term_taxonomy`
--
ALTER TABLE `term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`);

--
-- Indexes for table `type_of_discount`
--
ALTER TABLE `type_of_discount`
  ADD PRIMARY KEY (`type_of_discount_id`);

--
-- Indexes for table `variable_products`
--
ALTER TABLE `variable_products`
  ADD PRIMARY KEY (`variable_product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_portal`
--
ALTER TABLE `bank_portal`
  MODIFY `bank_potal_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
  MODIFY `discount_coupon_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_settings`
--
ALTER TABLE `email_settings`
  MODIFY `email_setting_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `front_general_settings`
--
ALTER TABLE `front_general_settings`
  MODIFY `front_general_setting_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `front_slider_settings`
--
ALTER TABLE `front_slider_settings`
  MODIFY `front_slider_setting_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grouping_products`
--
ALTER TABLE `grouping_products`
  MODIFY `group_product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `invoice_product`
--
ALTER TABLE `invoice_product`
  MODIFY `invoice_product_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `lable_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lables_products`
--
ALTER TABLE `lables_products`
  MODIFY `lable_product_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products_specifications_taxonomy`
--
ALTER TABLE `products_specifications_taxonomy`
  MODIFY `specifications_taxonomy_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products_specification_key`
--
ALTER TABLE `products_specification_key`
  MODIFY `specification_key_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products_specification_value`
--
ALTER TABLE `products_specification_value`
  MODIFY `product_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `property_key`
--
ALTER TABLE `property_key`
  MODIFY `property_key_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `property_key_products`
--
ALTER TABLE `property_key_products`
  MODIFY `property_key_product_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `property_value`
--
ALTER TABLE `property_value`
  MODIFY `property_value_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `related_products`
--
ALTER TABLE `related_products`
  MODIFY `related_product_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `rol_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `simple_products`
--
ALTER TABLE `simple_products`
  MODIFY `simple_product_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status_orders`
--
ALTER TABLE `status_orders`
  MODIFY `st_or_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `status_publication`
--
ALTER TABLE `status_publication`
  MODIFY `st_pu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `term_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `term_taxonomy`
--
ALTER TABLE `term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `type_of_discount`
--
ALTER TABLE `type_of_discount`
  MODIFY `type_of_discount_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `variable_products`
--
ALTER TABLE `variable_products`
  MODIFY `variable_product_id` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
